const User = {
    firstname: '',
    lastname: '',
    birthDate: '',
    city: '',
    address: '',
    postalCode: '',
    phoneNumber: '',
    email: '',
    startDate: new Date().toISOString().substr(0, 10),
    danceCourse: '',
    singCourse: '',
    paymentFoundation: '',
    age: '',
    amount: '', // prijs van cursus
    discount: false,
    if: false,
    familyDiscount: false,
    discountType: '',
    discountAmount: '',
    commantary: '',
    payment: {
      fullname: '',
      paymentAddress: '',
      automaticPaymentDate: '',
      iban: '',
      city: '',
      paymentPostalCode: '',
      paymentEmail: '',
      signature: {}
    },
    paymentMonth:
      {
        Jan: 'Niet Betaald',
        Feb: 'Niet Betaald',
        Maa: 'Niet Betaald',
        Apr: 'Niet Betaald',
        Mei: 'Niet Betaald',
        Jun: 'Niet Betaald',
        Jul: 'Niet Betaald',
        Aug: 'Niet Betaald',
        Sep: 'Niet Betaald',
        Okt: 'Niet Betaald',
        Nov: 'Niet Betaald',
        Dec: 'Niet Betaald'
      }
  }

  
  export default User
  