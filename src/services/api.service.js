import axios from 'axios'
import Config from '@/services/config.service'

axios.defaults.baseURL = Config.apiUrl


const ApiService = {
  register (data) {
    return new Promise((resolve, reject) => {
      axios.post('/user-register', data)
        .then(response => {
          resolve(response.data)
        }).catch(error => {
          reject(error)
        })
    })
  }
}

export default ApiService
